package com.epam.edu.hypermarket.controller;

import com.epam.edu.hypermarket.model.Product;
import com.epam.edu.hypermarket.model.SubCategory;
import java.util.List;

public interface Controllable {
  List<Product> getProductsForTypicalRepair()
      throws EmptyStorageException;
  List<Product> getProductsFromCategoryCheaperThan(SubCategory subCategory,
      double price) throws NoSuchProductException;
  List<Product> getProductsFromCategory(SubCategory subCategory)
      throws NoSuchProductException;
  List<Product> getProductsCheaperThan(double price)
      throws NoSuchProductException;
}
