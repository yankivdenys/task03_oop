package com.epam.edu.hypermarket.controller;

public class EmptyStorageException extends Exception {


  public EmptyStorageException(final String message) {
    super(message);
  }

}
