package com.epam.edu.hypermarket.controller;

import com.epam.edu.hypermarket.model.Product;
import com.epam.edu.hypermarket.model.Storage;
import com.epam.edu.hypermarket.model.SubCategory;

import java.util.ArrayList;
import java.util.List;

public class Controller implements Controllable {

  private Storage storage;

  public Controller(final Storage s) {
    this.storage = s;
  }

  @Override
  public final List<Product> getProductsForTypicalRepair()
      throws EmptyStorageException {
    if (storage.getList().isEmpty()) {
      throw new EmptyStorageException("storage is empty");
    }
    return storage.getList();
  }

  @Override
  public final List<Product> getProductsFromCategoryCheaperThan(
      final SubCategory subCategory,
      final double price) throws NoSuchProductException {
    List<Product> requestedProducts = new ArrayList<>();
    for (Product p : getProductsFromCategory(subCategory)) {
      if (p.getPrice() <= price) {
        requestedProducts.add(p);
      }
    }
    if (requestedProducts.isEmpty()) {
      throw new NoSuchProductException("no such products");
    }
    return requestedProducts;
  }

  @Override
  public final List<Product> getProductsFromCategory(
      final SubCategory subCategory) throws NoSuchProductException {
    List<Product> requestedProducts = new ArrayList<>();
    for (Product p : storage.getList()) {
      if (p.getSubCategory().equals(subCategory)) {
        requestedProducts.add(p);
      }
    }
    if (requestedProducts.isEmpty()) {
      throw new NoSuchProductException("no such products");
    }
    return requestedProducts;
  }

  @Override
  public List<Product> getProductsCheaperThan(final double price)
      throws NoSuchProductException {
    List<Product> requestedProducts = new ArrayList<>();
    for (Product p : storage.getList()) {
      if (p.getPrice() <= price) {
        requestedProducts.add(p);
      }
    }
    if (requestedProducts.isEmpty()) {
      throw new NoSuchProductException("no such products");
    }
    return requestedProducts;
  }
}
