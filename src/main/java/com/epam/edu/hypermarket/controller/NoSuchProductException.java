package com.epam.edu.hypermarket.controller;

public class NoSuchProductException extends Exception {

  public NoSuchProductException(final String message) {
    super(message);
  }
}