package com.epam.edu.hypermarket.model;

import java.util.ArrayList;
import java.util.List;

public class Storage {

  private List<Product> list;

  public Storage() {
    list = new ArrayList<>();
     list.add(new Product(20, 20, "super paint", SubCategory.PAINT));
     list.add(new Product(10, 120, "super VARNISH", SubCategory.VARNISH));
     list.add(new Product(16, 40, "super bowl3000", SubCategory.BOWL));
     list.add(new Product(23, 60, "perfect doors ultimate", SubCategory.DOOR));
     list.add(new Product(243, 100, "black laminate", SubCategory.LAMINATE));
     list.add(new Product(24, 120, "white and pink floor lux",
         SubCategory.FLOOR));
     list.add(new Product(249, 130, "elite washbasin", SubCategory.WASHBASIN));

  }

  public final List<Product> getList() {
    return list;
  }
}
