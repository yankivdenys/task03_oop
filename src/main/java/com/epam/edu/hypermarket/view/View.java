package com.epam.edu.hypermarket.view;

import com.epam.edu.hypermarket.controller.Controller;
import com.epam.edu.hypermarket.controller.EmptyStorageException;
import com.epam.edu.hypermarket.controller.NoSuchProductException;
import com.epam.edu.hypermarket.model.Product;
import com.epam.edu.hypermarket.model.Storage;
import com.epam.edu.hypermarket.model.SubCategory;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public final class View {

  private Controller controller;
  private Map<String, String> menu;
  private Map<String, Printable> methodsMenu;
  private static Scanner input = new Scanner(System.in, "UTF-8");

  public View() {
    controller = new Controller(new Storage());
    menu = new LinkedHashMap<>();
    menu.put("1", "1 - getProductsForTypicalRepair");
    menu.put("2", "2 - getProductsFromCategoryCheaperThan");
    menu.put("3", "3 - getProductsFromCategory");
    menu.put("4", "4 - getProductsCheaperThan");
    menu.put("Q", "Q - exit");

    methodsMenu = new LinkedHashMap<>();
    methodsMenu.put("1", this::pressButton1);
    methodsMenu.put("2", this::pressButton2);
    methodsMenu.put("3", this::pressButton3);
    methodsMenu.put("4", this::pressButton4);
  }

  private <E> void printList(final List<E> list) {
    for (E o : list) {
      System.out.println(o);
    }
  }

  private SubCategory subCategoryFromString(final String stringSubCategory) {
    SubCategory subCategory = SubCategory.OTHER;
    try {
      subCategory = SubCategory.valueOf(stringSubCategory.toUpperCase().trim());
    } catch (IllegalArgumentException e) {
      System.out.println("No such category. Setting to OTHER");
    }
    return subCategory;
  }

  private void pressButton1() {
    List<Product> list;
    try {
      list = controller.getProductsForTypicalRepair();
      printList(list);
    } catch (EmptyStorageException e) {
      System.out.println(e.getMessage());
    }
  }

  private void pressButton2() {
    System.out.println("Enter product category: ");
    SubCategory subCategory = subCategoryFromString(input.nextLine());
    System.out.println("Enter upper price level: ");
    double price = input.nextDouble();

    List<Product> list;
    try {
      list = controller.getProductsFromCategoryCheaperThan(subCategory, price);
      printList(list);
    } catch (NoSuchProductException e) {
      System.out.println(e.getMessage());
    }
  }

  private void pressButton3() {
    System.out.println("Enter product category: ");
    SubCategory subCategory = subCategoryFromString(input.nextLine());

    List<Product> list;
    try {
      list = controller.getProductsFromCategory(subCategory);
      printList(list);
    } catch (NoSuchProductException e) {
      System.out.println(e.getMessage());
    }
  }

  private void pressButton4() {
    System.out.println("Enter upper price level: ");
    double price = input.nextInt();

    List<Product> list;
    try {
      list = controller.getProductsCheaperThan(price);
      printList(list);
    } catch (NoSuchProductException e) {
      System.out.println(e.getMessage());
    }
  }

  private void outputMenu() {
    System.out.println("\nMenu:");
    for (String str : menu.values()) {
      System.out.println(str);
    }
  }

  public void show() {
    String keyMenu;
    do {
      outputMenu();
      System.out.println("Please, select menu point.");
      keyMenu = input.nextLine().toLowerCase();
      try {
        methodsMenu.get(keyMenu).print();
      } catch (Exception e) {
        System.out.println("Wrong input.");
      }
    } while (!keyMenu.equalsIgnoreCase("q"));
  }

}
